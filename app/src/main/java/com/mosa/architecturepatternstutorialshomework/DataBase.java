package com.mosa.architecturepatternstutorialshomework;

import com.mosa.architecturepatternstutorialshomework.pojo.NumberModel;

public class DataBase {
    public NumberModel getNumbers(){
        return new NumberModel(4, 2);
    }
}
