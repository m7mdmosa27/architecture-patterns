package com.mosa.architecturepatternstutorialshomework.ui;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.mosa.architecturepatternstutorialshomework.DataBase;
import com.mosa.architecturepatternstutorialshomework.R;
import com.mosa.architecturepatternstutorialshomework.pojo.NumberModel;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, DivView {

    TextView plus_result;
    Button bt_plus;

    //**************** using ButterKnife Libirary *************
    @BindView(R.id.div_result_textView)
    TextView div_result;
    @BindView(R.id.div_button)
    Button bt_div;
    @BindView(R.id.mul_result_textView)
    TextView mul_result;
    @BindView(R.id.mull_button)
    Button bt_mul;
    //*********************************************************

    // ************ MVP *************
    DivPresenter presenter;
    // ******************************
    MulViewModel mulViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        plus_result = findViewById(R.id.plus_result_textView);
        bt_plus = findViewById(R.id.plus_button);

        ButterKnife.bind(this);

        bt_plus.setOnClickListener(this);
        bt_div.setOnClickListener(this);
        bt_mul.setOnClickListener(this);

        //********************* Using MVP ********************
        presenter = new DivPresenter(this);
        //****************************************************

        // *************************** Using MVVM Architecture Pattern ************************
        mulViewModel= new ViewModelProvider(this).get(MulViewModel.class);
        mulViewModel.nums.observe(this, new Observer<NumberModel>() {
            @Override
            public void onChanged(NumberModel numberModel) {
                System.out.println("observer");
                Log.i("TAG", "onChanged: ");
                mul_result.setText(String.valueOf(numberModel.getFirstNum()
                        * numberModel.getSecondNum()));
            }
        });
        //*************************************************************************************

    }

    // ***************  using MVC Architecture Pattern  **************
    public void plusFunByMVC() {
        DataBase data = new DataBase();
        NumberModel model = data.getNumbers();
        int plus = model.getFirstNum() + model.getSecondNum();
        plus_result.setText(String.valueOf(plus));
    }
    //****************************************************************

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.plus_button){
            // ************ MVC ***************
            plusFunByMVC();
            //**********************************
        }else if (view.getId() == R.id.div_button){
            // ************ MVP ***************
            presenter.getNum();
            //*********************************
        }else if (view.getId() == R.id.mull_button){
            // ************ MVVM ***************
            mulViewModel.getNum();
            // *********************************
        }

    }

    //*************************** MVP ***************************
    @Override
    public void getNum(int first_num,int sec_num) {
        div_result.setText(String.valueOf(first_num/sec_num));
    }
    //***********************************************************

}
