package com.mosa.architecturepatternstutorialshomework.ui;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.mosa.architecturepatternstutorialshomework.DataBase;
import com.mosa.architecturepatternstutorialshomework.pojo.NumberModel;

public class MulViewModel extends ViewModel {

    MutableLiveData<NumberModel> nums =new MutableLiveData<>();

    public void getNum(){
        nums.setValue(getNumFromDataBase());
    }

    private NumberModel getNumFromDataBase(){
        DataBase data= new DataBase();
        return data.getNumbers();
    }
}
