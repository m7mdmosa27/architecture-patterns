package com.mosa.architecturepatternstutorialshomework.ui;

import android.view.View;

import com.mosa.architecturepatternstutorialshomework.DataBase;
import com.mosa.architecturepatternstutorialshomework.pojo.NumberModel;

public class DivPresenter {
    DivView view;

    public DivPresenter(DivView view) {
        this.view = view;
    }

    public NumberModel getModelFromDataBase(){
        DataBase data = new DataBase();
        NumberModel model = data.getNumbers();
        return model;
    }

    public void getNum(){
        view.getNum(getModelFromDataBase().getFirstNum(),
                getModelFromDataBase().getSecondNum());
    }

}
